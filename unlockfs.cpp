/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <limits.h>
#include <linux/input.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>
#include <dirent.h>

#include "common.h"
#include "cutils/properties.h"
#ifdef ANDROID_RB_RESTART
#include "cutils/android_reboot.h"
#else
#include <sys/reboot.h>
#endif
#include "minuitwrp/minui.h"
#include "minzip/DirUtil.h"

extern "C" {
#include "data.h"
#include "gui/gui.h"
}
#include "partitions.hpp"
#include "variables.h"
#include "twrp-functions.hpp"

TWPartitionManager PartitionManager;

static const char *LOG_FILE = "/cache/recovery/unlockfs.log";
static const char *TEMPORARY_LOG_FILE = "/tmp/unlockfs.log";

static const int MAX_ARG_LENGTH = 4096;
static const int MAX_ARGS = 100;

int
main(int argc, char **argv) {
    // Recovery needs to install world-readable files, so clear umask
    // set by init
    umask(0);
    
    time_t start = time(NULL);

    //remove(TEMPORARY_LOG_FILE);
    
    // If these fail, there's not really anywhere to complain...
    freopen(TEMPORARY_LOG_FILE, "a", stdout); setbuf(stdout, NULL);
    freopen(TEMPORARY_LOG_FILE, "a", stderr); setbuf(stderr, NULL);
    
    LOGI("Starting UnlockFS %s on %s\n", "1.0", ctime(&start));
    
	// Load default values to set DataManager constants and handle ifdefs
	DataManager_LoadDefaults();
        
    char keyfile[PROPERTY_VALUE_MAX] = "";
    property_get("ro.crypto.keyfile.userdata", keyfile, "");
    if(!keyfile[0])
    {
        LOGI("Encryption not detected: ro.crypto.keyfile.userdata=%s\n", keyfile);
        LOGI(">> Exiting UnlockFS\n");
        return 0;
    }

    LOGI("Encryption detected: ro.crypto.keyfile.userdata=%s\n", keyfile);
    
	DataManager_SetStrValue("tw_crypto_password", "");
	DataManager_SetStrValue("tw_crypto_display", "");
    DataManager_SetIntValue(TW_IS_ENCRYPTED, 1);

	printf("Starting the UI...");
	gui_init();
	printf("=> Linking mtab\n");
	symlink("/proc/mounts", "/etc/mtab");
	printf("=> Processing recovery.fstab\n");
	if (!PartitionManager.Process_Fstab("/etc/recovery.fstab", true)) {
		LOGE("Failing out of UnlockFS due to problem with /etc/recovery.fstab.\n");
		return -1;
	}
	
	//PartitionManager.Output_Partition_Logging();
	//PartitionManager.Mount_All();
    //PartitionManager.Write_FstabEx("/etc/recovery.fstab");

	// Load up all the resources
	printf("=> gui_loadResources()\n");
	gui_loadResources();

    gui_console_only();

	printf("=> gui_start()\n");
    gui_start();

    return EXIT_SUCCESS;
}
